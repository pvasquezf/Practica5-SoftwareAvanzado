const { menus } = require('./helpers');
test('20 Menus encontrados', () => {
    expect(menus.length).toBe(20);
    expect(menus[2].id).toBe("a2983cc4-7933-461a-b47d-8009f6e14b73");
    expect(menus[2].description).toBe("Menu 3");
    expect(menus[2].price).toBe(431);
});
test('Id de menu 3', () => {
    expect(menus[2].id).toBe("a2983cc4-7933-461a-b47d-8009f6e14b73");
});
test('Nombre de menu 3', () => {
    expect(menus[2].description).toBe("Menu 3");
});
test('PRecio del menu 3', () => {
    expect(menus[2].price).toBe(431);
});